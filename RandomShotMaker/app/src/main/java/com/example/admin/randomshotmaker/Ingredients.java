package com.example.admin.randomshotmaker;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Admin on 14.07.2018.
 */

public class Ingredients
{
    public static List<String> ingredientsList = new ArrayList<String>();
    public static List<String> alcoholList = new ArrayList<String>();

    public static void initTest()
    {
        Ingredients.addIngredient("wasser", false);
        Ingredients.addIngredient("dominiks Spucke", true);
        Ingredients.addIngredient("vodka ", true);
        Ingredients.addIngredient("cola", false);
        Ingredients.addIngredient("pfeffer", false);
        Ingredients.addIngredient("tomatensaft", false);
        Ingredients.addIngredient("reinigungsalkohol", true);
        Ingredients.addIngredient("salz", false);
    }
    public static void main(String[] args)
    {
        Ingredients.initTest();
    }
    public static void addIngredient(String ingredient, boolean isAlcohol)
    {
        if(!ingredientsList.contains(ingredient))
        {
            ingredientsList.add(ingredient);
            if(isAlcohol)
                alcoholList.add(ingredient);
        }
    }
    public static void deleteIngredient(String ingredient)
    {
        if(ingredientsList.contains(ingredient))
        {
            ingredientsList.remove(ingredient);
            if(alcoholList.contains(ingredient))
                alcoholList.remove(ingredient);
        }
    }

    public static String[] getRandomShot(int minAmount, int maxAmount, boolean withAlcohol)
    {
        Random rnd = new Random();
        int amount = rnd.nextInt(maxAmount-minAmount) + minAmount;
        int startIndex = 0;
        int ingredientIndex;
        String[] shotIngredients = new String[amount];
        if(amount>0)
        {
            if (withAlcohol)
            {
                amount--;
                startIndex++;
                ingredientIndex = rnd.nextInt(alcoholList.size());
                shotIngredients[0] = alcoholList.get(ingredientIndex);
            }
            for (int i = startIndex; i < shotIngredients.length; i++)
            {
                ingredientIndex = rnd.nextInt(ingredientsList.size());
                shotIngredients[i] = ingredientsList.get(ingredientIndex);
            }
        }
        return shotIngredients;
    }
}
