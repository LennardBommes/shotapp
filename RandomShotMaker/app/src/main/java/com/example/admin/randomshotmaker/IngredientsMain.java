package com.example.admin.randomshotmaker;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class IngredientsMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredients_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button fab = (Button) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Ingredients.initTest();
                String output = "";
                String[] outputArray = Ingredients.getRandomShot(2,5,true);
                int items = outputArray.length;
                ((TextView)findViewById(R.id.txbIn1)).setText(outputArray[0]);
                ((TextView)findViewById(R.id.txbIn2)).setText(outputArray[1]);
                if(items>2)
                    ((TextView)findViewById(R.id.txbIn3)).setText(outputArray[2]);
                if(items>3)
                    ((TextView)findViewById(R.id.txbIn4)).setText(outputArray[3]);
                if(items>4)
                    ((TextView)findViewById(R.id.txbIn5)).setText(outputArray[4]);

            }
        });

        Button btnOpenAdd  = (Button) findViewById(R.id.btnAdd);
             btnOpenAdd.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {


                        Snackbar.make(view, "added", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();

                    }
                });
    }

    private void openAddActivity()
    {
        Intent addActivity  = new Intent(this, IngredientsActivity.class);
        startActivity(addActivity);
    }
 /*
    private void addIngredient()
    {
        EditText ingredientTextBox = (EditText)findViewById(R.id.txbIngedient);
        String ingridientText = ingredientTextBox.getText().toString();
        Ingredients.addIngredient(ingridientText,false);
    }
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ingredients_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
